window.onload = function () {


    const links = document.querySelectorAll('.link');
    function activeRemoval (){  
        for(i = 0; i < links.length; i++){
            links[i].classList.remove('active');
        }
    }

    const panels = document.querySelectorAll('.panel');
    function hiddenAll(){      
        for(i = 0; i < panels.length; i++){
            panels[i].style.display = 'none';
        }
    }

    function addPanels(){
        hiddenAll();
        activeRemoval();
        this.classList.add('active');
        var elementId = this.id.replace('-link', '');
        var panelShown = document.getElementById(elementId);
        panelShown.style.display = 'block';
    }


    for(i = 0; i < links.length; i++){
        links[i].addEventListener('click', addPanels);
    }

}